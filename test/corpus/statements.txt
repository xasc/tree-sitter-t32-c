============================================
If statements
============================================

(a);
  1;

(!a);
  2;

  3;


---

(translation_unit
  (expression_statement
    (parenthesized_expression
      (identifier)))
  (expression_statement
    (number_literal))
  (expression_statement
    (parenthesized_expression
      (unary_expression
        (identifier))))
  (expression_statement
    (number_literal))
  (expression_statement
    (number_literal)))


============================================
For loops
============================================

    1;

  int i = 0; i < 5; next(), i++;
    2;

  start(); check(); step();
    3;

  ji = 0, j = 0, k = 0, l = 0; i < 1, j < 1; i++, j++, k++, l++;
    1;

---

(translation_unit
  (expression_statement
    (number_literal))
  (declaration
    (primitive_type)
      (init_declarator
        (identifier)
        (number_literal)))
  (expression_statement
    (binary_expression
      (identifier)
      (number_literal)))
  (expression_statement
    (comma_expression
      (call_expression
        (identifier)
        (argument_list))
      (update_expression
        (identifier))))
  (expression_statement
    (number_literal))
  (expression_statement
    (call_expression
      (identifier)
      (argument_list)))
  (expression_statement
    (call_expression
      (identifier)
        (argument_list)))
  (expression_statement
    (call_expression
      (identifier)
      (argument_list)))
  (expression_statement
    (number_literal))
  (expression_statement
    (comma_expression
      (assignment_expression
        (identifier)
        (number_literal))
      (comma_expression
        (assignment_expression
          (identifier)
          (number_literal))
        (comma_expression
          (assignment_expression
            (identifier)
            (number_literal))
          (assignment_expression
            (identifier)
            (number_literal))))))
  (expression_statement
    (comma_expression
      (binary_expression
        (identifier)
        (number_literal))
      (binary_expression
        (identifier)
        (number_literal))))
  (expression_statement
    (comma_expression
      (update_expression
        (identifier))
      (comma_expression
        (update_expression
          (identifier))
        (comma_expression
          (update_expression
            (identifier))
          (update_expression
            (identifier))))))
  (expression_statement
    (number_literal)))


============================================
While loops
============================================

  printf("hi");

---

(translation_unit
  (expression_statement
    (call_expression
      (identifier)
      (argument_list
        (string_literal)))))


============================================
Labeled statements
============================================

  t = t->next();

---

(translation_unit
  (expression_statement
    (assignment_expression
      (identifier)
      (call_expression
        (field_expression
          (identifier)
          (field_identifier))
        (argument_list)))))


============================================
Return statements
============================================

a;
a, b;

---

(translation_unit
  (expression_statement
    (identifier))
  (expression_statement
    (comma_expression
      (identifier)
      (identifier))))


============================================
Comments with asterisks
============================================

/*************************
 * odd number of asterisks
 *************************/
int a;

/**************************
 * even number of asterisks
 **************************/
int b;

---

(translation_unit
  (comment)
  (declaration (primitive_type) (identifier))
  (comment)
  (declaration (primitive_type) (identifier)))
